package ru.tsc.babeshko.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String TaskId);

    void unbindTaskFromProject(String userId, String projectId, String TaskId);

    void removeProjectById(String userId, String projectId);

}
