package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task>{

    Task create(String userId, String name);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task create(String userId, String name, String description);

}