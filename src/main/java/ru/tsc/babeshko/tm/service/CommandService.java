package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.repository.ICommandRepository;
import ru.tsc.babeshko.tm.api.service.ICommandService;
import ru.tsc.babeshko.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public void add(final AbstractCommand command) {
        Optional.ofNullable(command)
                .ifPresent(commandRepository::add);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (!Optional.ofNullable(name)
                .isPresent()) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (!Optional.ofNullable(argument)
                .isPresent()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

}
