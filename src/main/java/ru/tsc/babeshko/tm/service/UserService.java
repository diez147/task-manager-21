package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.repository.IUserRepository;
import ru.tsc.babeshko.tm.api.service.IUserService;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.exception.entity.user.EmailExistsException;
import ru.tsc.babeshko.tm.exception.entity.user.LoginExistsException;
import ru.tsc.babeshko.tm.exception.entity.user.UserNotFoundException;
import ru.tsc.babeshko.tm.exception.field.*;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(IUserRepository repository) {
        super(repository);
    }

    @Override
    public User findByLogin(String login) {
        Optional.ofNullable(login)
                .orElseThrow(LoginEmptyException::new);
        return repository.findByLogin(login);
    }

     @Override
    public User removeByLogin(String login) {
         Optional.ofNullable(login)
                 .orElseThrow(LoginEmptyException::new);
         return repository.removeByLogin(login);
    }

    @Override
    public User findByEmail(String email) {
        Optional.ofNullable(email)
                .orElseThrow(EmailEmptyException::new);
        return repository.findByEmail(email);
    }

    @Override
    public User create(String login, String password) {
        Optional.ofNullable(login)
                .orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password)
                .orElseThrow(PasswordEmptyException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return repository.add(user);
    }

    @Override
    public User create(String login, String password, String email) {
        Optional.ofNullable(login)
                .orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password)
                .orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(email)
                .orElseThrow(EmailEmptyException::new);
        if (isEmailExist(email)) throw new EmailExistsException();
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = create(login, password);
        Optional.ofNullable(user)
                .orElseThrow(UserNotFoundException::new);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(String login, String password, Role role) {
        Optional.ofNullable(login)
                .orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password)
                .orElseThrow(PasswordEmptyException::new);
        Optional.ofNullable(role)
                .orElseThrow(RoleEmptyException::new);
        if (isLoginExist(login)) throw new LoginExistsException();
        final User user = create(login, password);
        Optional.ofNullable(user)
                .orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return user;
    }

    @Override
    public User setPassword(String userId, String password) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(password)
                .orElseThrow(PasswordEmptyException::new);
        final User user = findOneById(userId);
        Optional.ofNullable(user)
                .orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(HashUtil.salt(password));
        return user;

    }

    @Override
    public User updateUser(String userId, String firstName, String lastName, String middleName) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        final User user = findOneById(userId);
        Optional.ofNullable(user)
                .orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public boolean isLoginExist(String login) {
        if (!Optional.ofNullable(login)
                .isPresent()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(String email) {
        if (!Optional.ofNullable(email)
                .isPresent()) return false;
        return findByEmail(email) != null;
    }

}