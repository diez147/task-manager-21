package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.repository.ITaskRepository;
import ru.tsc.babeshko.tm.api.service.ITaskService;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.exception.entity.project.ProjectNotFoundException;
import ru.tsc.babeshko.tm.exception.entity.task.TaskNotFoundException;
import ru.tsc.babeshko.tm.exception.field.*;
import ru.tsc.babeshko.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(ITaskRepository repository) {
        super(repository);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId)
                .orElseThrow(ProjectNotFoundException::new);
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task create(final String userId, final String name) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name)
                .orElseThrow(NameEmptyException::new);
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(name)
                .orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description)
                .orElseThrow(DescriptionEmptyException::new);
        return repository.create(userId, name, description);
    }

    @Override
    public Task create(final String userId, final String name, final String description, final Date dateBegin, final Date dateEnd) {
        final Task task = Optional.ofNullable(create(userId, name, description))
                .orElseThrow(TaskNotFoundException::new);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id)
                .orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name)
                .orElseThrow(NameEmptyException::new);
        final Task task = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        Optional.ofNullable(name)
                .orElseThrow(NameEmptyException::new);
        final Task task = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id)
                .orElseThrow(IdEmptyException::new);
        final Task task = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        Optional.ofNullable(userId)
                .orElseThrow(UserIdEmptyException::new);
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        final Task task = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status);
        task.setUserId(userId);
        return task;
    }

}