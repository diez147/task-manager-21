package ru.tsc.babeshko.tm.service;

import ru.tsc.babeshko.tm.api.service.IAuthService;
import ru.tsc.babeshko.tm.api.service.IUserService;
import ru.tsc.babeshko.tm.enumerated.Role;
import ru.tsc.babeshko.tm.exception.entity.user.AccessDeniedException;
import ru.tsc.babeshko.tm.exception.entity.user.PermissionException;
import ru.tsc.babeshko.tm.exception.field.LoginEmptyException;
import ru.tsc.babeshko.tm.exception.field.PasswordEmptyException;
import ru.tsc.babeshko.tm.exception.field.UserIdEmptyException;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;

public class AuthService implements IAuthService {

    private final IUserService userService;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    private String userId;

    @Override
    public void login(String login, String password) {
        Optional.ofNullable(login)
                .orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(password)
                .orElseThrow(PasswordEmptyException::new);
        final User user = Optional.ofNullable(userService.findByLogin(login))
                .filter(m -> m.getPasswordHash().equals(HashUtil.salt(password)))
                .orElseThrow(AccessDeniedException::new);
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(String login, String password, String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        Optional.ofNullable(userId)
                .orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public void checkRoles(final Role[] roles){
        if (!Optional.ofNullable(roles)
                .isPresent()) return;
        final User user = getUser();
        final Role role = user.getRole();
        Optional.ofNullable(role)
                .orElseThrow(PermissionException::new);
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

}