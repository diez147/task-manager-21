package ru.tsc.babeshko.tm.command.system;

import ru.tsc.babeshko.tm.api.service.ICommandService;
import ru.tsc.babeshko.tm.command.AbstractCommand;
import ru.tsc.babeshko.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
