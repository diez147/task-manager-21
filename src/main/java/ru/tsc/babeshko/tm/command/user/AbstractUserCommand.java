package ru.tsc.babeshko.tm.command.user;

import ru.tsc.babeshko.tm.api.service.IAuthService;
import ru.tsc.babeshko.tm.api.service.IUserService;
import ru.tsc.babeshko.tm.command.AbstractCommand;
import ru.tsc.babeshko.tm.exception.entity.user.UserNotFoundException;
import ru.tsc.babeshko.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    public IAuthService getAuthService() {
        return getServiceLocator().getAuthService();
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
    }

}