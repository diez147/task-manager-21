package ru.tsc.babeshko.tm.repository;

import ru.tsc.babeshko.tm.api.repository.IUserRepository;
import ru.tsc.babeshko.tm.model.User;


public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByEmail(String email) {
        return models.stream()
                .filter(m -> email.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByLogin(String login) {
        return models.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

     @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        models.remove(user);
        return user;
    }

}