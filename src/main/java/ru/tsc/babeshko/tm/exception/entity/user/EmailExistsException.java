package ru.tsc.babeshko.tm.exception.entity.user;

import ru.tsc.babeshko.tm.exception.entity.AbstractEntityNotFoundException;

public final class EmailExistsException extends AbstractEntityNotFoundException {

    public EmailExistsException() {
        super("Error! Email already exist...");
    }

}