package ru.tsc.babeshko.tm.exception.entity.user;

import ru.tsc.babeshko.tm.exception.AbstractException;

public class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Role is incorrect...");
    }

}
