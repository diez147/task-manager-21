package ru.tsc.babeshko.tm.exception.entity.user;

import ru.tsc.babeshko.tm.exception.entity.AbstractEntityNotFoundException;

public final class LoginExistsException extends AbstractEntityNotFoundException {

    public LoginExistsException() {
        super("Error! Login already exist...");
    }

}