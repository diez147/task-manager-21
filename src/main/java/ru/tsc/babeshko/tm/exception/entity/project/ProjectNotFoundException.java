package ru.tsc.babeshko.tm.exception.entity.project;

import ru.tsc.babeshko.tm.exception.entity.AbstractEntityNotFoundException;

public final class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
