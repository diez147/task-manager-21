package ru.tsc.babeshko.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! User ID is empty...");
    }

}